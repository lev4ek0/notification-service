from django.db.models import Count
from rest_framework import serializers

from mailing_app.models import Client, Mailing, Message
from utils.validations import phone_number_validation


class ClientSerializer(serializers.ModelSerializer):
    operator_code = serializers.ReadOnlyField()

    @staticmethod
    def validate_phone_number(value: str) -> str:
        phone_number_validation(value=value)
        return value

    class Meta:
        model = Client
        fields = ("id", "phone_number", "operator_code", "tag", "time_zone")


class MailingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Mailing
        fields = ("id", "start_at", "finish_at", "filter", "text")


class StatisticsSerializer(serializers.ModelSerializer):
    messages = serializers.SerializerMethodField()

    @staticmethod
    def get_messages(obj):
        return (
            Message.objects.filter(mailing_id=obj.pk)
            .values("status")
            .annotate(count=Count("status"))
            .order_by("-count")
        )

    class Meta:
        model = Mailing
        fields = ("id", "text", "messages")
