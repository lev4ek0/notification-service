from datetime import datetime

import pytz
from django.conf import settings
from rest_framework.generics import ListAPIView, RetrieveAPIView

from mailing_app.models import Mailing
from mailing_app.serializers import StatisticsSerializer


class StatisticsAPIView(ListAPIView):
    serializer_class = StatisticsSerializer

    def get_queryset(self):
        return Mailing.objects.all()


class StatisticsDetailAPIView(RetrieveAPIView):
    serializer_class = StatisticsSerializer

    def get_queryset(self):
        return Mailing.objects.all()


class StatisticsActiveAPIView(ListAPIView):
    serializer_class = StatisticsSerializer

    def get_queryset(self):
        dt_now = datetime.now(tz=pytz.timezone(settings.TIME_ZONE))
        return Mailing.objects.filter(start_at__lte=dt_now, finish_at__gte=dt_now)
