from django.test import Client as TestClient, TestCase
from rest_framework import status


class MailingTestCase(TestCase):
    def setUp(self):
        super().setUp()
        self.client = TestClient()

    def test_client_creation(self):
        response = self.client.post(
            "/api/v1/clients/",
            {
                "tag": "test",
                "phone_number": "73922220934",
                "time_zone": "Europe/Moscow",
            },
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
