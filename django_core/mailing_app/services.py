from datetime import datetime

import celery
import pytz
import requests
from celery.exceptions import MaxRetriesExceededError
from celery.result import AsyncResult
from django.conf import settings
from django.core.cache import caches
from django.db.models import Q, QuerySet
from requests import Response
from rest_framework import status

from django_core.celery import app
from mailing_app.choices import MessageStatus
from mailing_app.models import Client, Message, Mailing


__all__ = [
    "send_mailing_to_all",
    "make_cancelled_status",
    "send_mailing_to_client",
    "mailing_stop",
]

session = requests.Session()

cache_send_client = caches["executing_tasks_send_client"]
cache_send_all = caches["executing_tasks_send_all"]


def _get_new_clients_for_mailing(*, mailing: Mailing) -> QuerySet[Client]:
    """
    Возвращает список объектов клиентов, которым нужно отправить сообщения
    :param mailing: Объект рассылки
    :return: Список объектов клиентов
    """
    return Client.objects.filter(
        (Q(operator_code=mailing.filter) | Q(tag=mailing.filter))
    ).exclude(messages__mailing=mailing)


def _create_messages_for_sending(*, mailing_id: int, clients: QuerySet[Client]):
    Message.objects.bulk_create(
        [
            Message(
                client_id=client.pk,
                mailing_id=mailing_id,
                status=MessageStatus.INITIALIZED,
            )
            for client in clients
        ]
    )


def _get_messages_for_sending(*, mailing: Mailing) -> QuerySet[Message]:
    """
    Возвращает список объектов сообщений, подходящих по условию
    :param mailing: Объект рассылки
    :return: Список объектов сообщений
    """
    return Message.objects.select_related("mailing", "client").filter(
        mailing=mailing,
        status__in=[
            MessageStatus.ABORTED,
            MessageStatus.INITIALIZED,
            MessageStatus.CANCELLED,
        ],
    )


def _make_api_request(*, message_id: int, phone_number: str, text: str) -> Response:
    return session.post(
        f"{settings.MAILING_API_URL}{message_id}",
        json={"id": message_id, "phone": int(phone_number), "text": text},
        headers={"Authorization": settings.MAILING_AUTHORIZATION_TOKEN},
    )


def _send_mailing_by_messages(*, messages: QuerySet[Message], mailing: Mailing) -> None:
    """
    Запускает задачу отправки сообщений клиентам
    :param messages: Список объектов сообщений
    :param mailing: Объект рассылки
    :return:
    """
    for message in messages:
        result: AsyncResult = celery.current_app.send_task(
            name="mailing_app.tasks.send_client",
            kwargs={
                "text": message.mailing.text,
                "phone_number": message.client.phone_number,
                "message_id": message.pk,
                "finish_at": mailing.finish_at.isoformat(),
            },
            expires=mailing.finish_at,
        )
        dt_now = datetime.now(tz=pytz.timezone(settings.TIME_ZONE))
        cache_send_client.set(
            message.pk, result.task_id, timeout=(mailing.finish_at - dt_now).seconds
        )


def _handle_response_status(
    *, self, response: Response, finish_at: str, message: Message
) -> None:
    """
    Обработка ответа стороннего сервера
    :param self: Объект задачи
    :param response: Ответ сервера
    :param finish_at: Время окончания рассылки
    :param message: Объект ообщения клиенту
    :return:
    """
    if (
        response.status_code == status.HTTP_200_OK
        and response.content == b'{"code":0,"message":"OK"}'
    ):
        message.status = MessageStatus.DELIVERED
        message.save()
    elif datetime.fromisoformat(finish_at) > datetime.now(
        tz=pytz.timezone(settings.TIME_ZONE)
    ):
        message.status = MessageStatus.PENDING
        message.save()
        try:
            self.retry(countdown=60)
        except MaxRetriesExceededError:
            message.status = MessageStatus.ABORTED
            message.save()


def send_mailing_to_all(*, self, mailing_id: int) -> None:
    """
    Отправляет сообщение всем пользователям, подходящим под рассылку
    :param self: Объект задачи
    :param mailing_id: Идентификатор рассылки
    :return:
    """
    mailing = Mailing.objects.get(pk=mailing_id)
    clients = _get_new_clients_for_mailing(mailing=mailing)
    _create_messages_for_sending(mailing_id=mailing_id, clients=clients)

    messages = _get_messages_for_sending(mailing=mailing)
    _send_mailing_by_messages(mailing=mailing, messages=messages)

    if mailing.finish_at > datetime.now(tz=pytz.timezone(settings.TIME_ZONE)):
        self.retry(countdown=60 * 60)


def make_cancelled_status(*, message_id: int) -> None:
    """
    Указывает отмененный статус сообщения
    :param message_id: Идентификатор сообщения клиенту
    :return:
    """
    message = Message.objects.get(pk=message_id)
    message.status = MessageStatus.CANCELLED
    message.save()


def send_mailing_to_client(
    *, self, text: str, phone_number: str, message_id: int, finish_at: str
) -> None:
    """
    Отправляет сообщение клиенту
    :param self: Объект задачи
    :param text: Сообщение рассылки
    :param phone_number: Номер телефона клиента
    :param message_id: Идентификатор сообщения клиенту
    :param finish_at: Время окончания рассылки
    :return:
    """
    response = _make_api_request(
        text=text, phone_number=phone_number, message_id=message_id
    )
    message = Message.objects.get(pk=message_id)
    _handle_response_status(
        self=self, message=message, response=response, finish_at=finish_at
    )


def mailing_stop(*, mailing_id: int) -> None:
    """
    Позволяет остановить рассылку сообщений
    :param mailing_id: Идентификатор рассылки
    :return:
    """
    messages = Message.objects.get(
        mailing_id=mailing_id,
        status__in=[MessageStatus.PENDING, MessageStatus.INITIALIZED],
    )
    for message in messages:
        task_id = cache_send_client.get(message.pk)
        app.control.revoke(task_id=task_id, terminate=True)
