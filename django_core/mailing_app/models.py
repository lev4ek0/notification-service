from datetime import datetime

import celery
import pytz
from celery.result import AsyncResult
from django.conf import settings
from django.core.cache import caches
from django.db import models
from django.utils.translation import gettext as _
from django_lifecycle import (
    LifecycleModel,
    hook,
    BEFORE_UPDATE,
    BEFORE_CREATE,
    AFTER_UPDATE,
    AFTER_CREATE,
)

from django_core.celery import app
from mailing_app.choices import MessageStatus
from utils.basemodel import BaseModel

cache = caches["executing_tasks_send_all"]


class Mailing(BaseModel):
    start_at = models.DateTimeField(
        verbose_name=_("Date and time of the mailing launch")
    )
    finish_at = models.DateTimeField(
        verbose_name=_("Date and time of the mailing ending")
    )
    filter = models.CharField(verbose_name=_("Mailing filtration"), max_length=255)
    text = models.TextField(verbose_name=_("Text of the message"))

    messages: models.Manager

    @hook(AFTER_CREATE)
    @hook(AFTER_UPDATE, when_any=["start_at", "finish_at"], has_changed=True)
    def start_mailing(self):
        task_id = cache.get(self.pk)
        app.control.revoke(task_id=task_id, terminate=True)
        result: AsyncResult = celery.current_app.send_task(
            "mailing_app.tasks.send_all",
            kwargs={"mailing_id": self.pk},
            eta=self.start_at,
            expires=self.finish_at,
        )
        dt_now = datetime.now(tz=pytz.timezone(settings.TIME_ZONE))
        cache.set(self.pk, result.task_id, timeout=(self.finish_at - dt_now).seconds)

    def __str__(self) -> str:
        return self.text

    class Meta(BaseModel.Meta):
        verbose_name = _("Mailing")
        verbose_name_plural = _("Mailings")


class Client(BaseModel):
    phone_number = models.CharField(
        verbose_name=_("Client phone number"), max_length=11, unique=True
    )
    operator_code = models.CharField(
        verbose_name=_("Code of mobile operator"), max_length=3
    )
    tag = models.CharField(verbose_name=_("Arbitrary label"), max_length=255)
    time_zone = models.CharField(
        verbose_name=_("Client time zone"), max_length=100, default="Europe/Moscow"
    )

    messages: models.Manager

    @hook(BEFORE_CREATE)
    @hook(BEFORE_UPDATE, when="phone_number", has_changed=True)
    def change_operator_code(self):
        self.operator_code = self.phone_number[1:4]

    def __str__(self) -> str:
        return self.phone_number

    class Meta(BaseModel.Meta):
        verbose_name = _("Client")
        verbose_name_plural = _("Clients")


class Message(BaseModel):
    client: Client = models.ForeignKey(
        Client,
        verbose_name=_("Client"),
        on_delete=models.CASCADE,
        related_name="messages",
    )
    mailing: Mailing = models.ForeignKey(
        Mailing,
        verbose_name=_("Mailing"),
        on_delete=models.CASCADE,
        related_name="messages",
    )
    status = models.CharField(
        max_length=15,
        choices=MessageStatus.choices,
        default=MessageStatus.PENDING,
        verbose_name=_("Message status"),
    )

    def __str__(self) -> str:
        return f"{self.client} - {self.mailing}"

    class Meta(BaseModel.Meta):
        verbose_name = _("Message")
        verbose_name_plural = _("Messages")
