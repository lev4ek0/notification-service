from celery.app.task import Context
from celery.signals import task_revoked

from django_core.celery import app
from mailing_app.services import (
    send_mailing_to_all,
    make_cancelled_status,
    send_mailing_to_client,
    mailing_stop,
)


@app.task(bind=True, retry_kwargs={"max_retries": 5})
def send_client(self, text, phone_number, message_id, finish_at):
    send_mailing_to_client(
        self=self,
        text=text,
        phone_number=phone_number,
        message_id=message_id,
        finish_at=finish_at,
    )


@task_revoked.connect(sender=send_client)
def task_revoked_handler(request: Context, *args, **kwargs):
    message_id = request.kwargs.get("message_id")
    make_cancelled_status(message_id=message_id)


@app.task(bind=True)
def send_all(self, mailing_id):
    send_mailing_to_all(self=self, mailing_id=mailing_id)


@task_revoked.connect(sender=send_all)
def task_revoked_handler(request: Context, *args, **kwargs):
    mailing_id = request.kwargs.get("mailing_id")
    mailing_stop(mailing_id=mailing_id)
