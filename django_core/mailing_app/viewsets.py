from rest_framework.viewsets import ModelViewSet

from mailing_app.models import Client, Mailing
from mailing_app.serializers import ClientSerializer, MailingSerializer


class ClientViewSet(ModelViewSet):
    serializer_class = ClientSerializer
    queryset = Client.objects.all()


class MailingViewSet(ModelViewSet):
    serializer_class = MailingSerializer
    queryset = Mailing.objects.all()
