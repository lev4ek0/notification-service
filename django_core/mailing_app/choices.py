from django.db import models
from django.utils.translation import gettext as _


class MessageStatus(models.TextChoices):
    DELIVERED = "DELIVERED", _("Delivered")
    CANCELLED = "CANCELLED", _("Cancelled")
    PENDING = "PENDING", _("Pending")
    ABORTED = "ABORTED", _("Aborted")
    INITIALIZED = "INITIALIZED", _("Initialized")
