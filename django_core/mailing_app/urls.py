from django.urls import path
from rest_framework.routers import DefaultRouter

from mailing_app.viewsets import ClientViewSet, MailingViewSet
from mailing_app.views import (
    StatisticsAPIView,
    StatisticsDetailAPIView,
    StatisticsActiveAPIView,
)

router = DefaultRouter()
router.register("clients", ClientViewSet)
router.register("mailings", MailingViewSet)
view_set_urls = router.urls

view_urls = [
    path("statistics/", StatisticsAPIView.as_view()),
    path("statistics/<int:pk>/", StatisticsDetailAPIView.as_view()),
    path("statistics/active/", StatisticsActiveAPIView.as_view()),
]

urlpatterns = view_urls + view_set_urls
