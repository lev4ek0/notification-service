from django.db import models
from django_lifecycle import LifecycleModel


class BaseModel(LifecycleModel):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ("created_at",)
        abstract = True
