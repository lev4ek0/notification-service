from django.utils.translation import gettext as _
from rest_framework import serializers


def phone_number_validation(*, value) -> None:
    if len(value) != 11:
        raise serializers.ValidationError(
            _("Client's phone number length must be equal to 11")
        )
    if not value.startswith("7"):
        raise serializers.ValidationError(_("Client's phone number must start with 7"))
    if not value.isdigit():
        raise serializers.ValidationError(_("Client's phone number must be a number"))
