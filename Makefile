up:
	cp django_core/production.example.env django_core/.env
	mkdir -p django_core/mailing_app/migrations
	touch django_core/mailing_app/migrations/__init__.py
	docker-compose build
	docker-compose up
